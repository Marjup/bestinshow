import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';
import {LoginComponent} from './user/account/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SignupComponent} from './user/account/signup.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {UsersComponent} from './user/users.component';
import {UserService} from './services/user.service';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {UserListComponent} from './user/user-list/user-list.component';
import {AuthService} from './user/account/auth.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './user/account/auth.interceptor';
import {AccordionModule, AlertModule, BsDatepickerModule, ModalModule, TooltipModule} from 'ngx-bootstrap';
import {AuthGuard} from './user/account/auth-guard.service';
import {UserNewComponent} from './user/user-new/user-new.component';
import {KennelService} from './services/kennel.service';
import {BreedService} from './services/breed.service';
import {KennelApplicationComponent} from './kennel/kennel-application/kennel-application.component';
import {KennelNewComponent} from './kennel/kennel-new/kennel-new.component';
import {KennelListComponent} from './kennel/kennel-list/kennel-list.component';
import {KennelEditComponent} from './kennel/kennel-edit/kennel-edit.component';
import {DogListComponent} from './dog/dog-list/dog-list.component';
import {DogService} from './services/dog.service';
import {DogNewComponent} from './dog/dog-new/dog-new.component';
import {DogEditComponent} from './dog/dog-edit/dog-edit.component';
import {LitterListComponent} from './litter/litter-list/litter-list.component';
import {LitterNewComponent} from './litter/litter-new/litter-new.component';
import {CollectionListComponent} from './collection/collection-list/collection-list.component';

/** All routing info */
const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'users', component: UsersComponent, children: [
    { path: '', component: UserListComponent},
    { path: 'new', component: UserNewComponent},
    { path: ':id/edit', component: UserEditComponent}
  ], canActivate: [AuthGuard]},
  { path: 'applications', component: KennelApplicationComponent, canActivate: [AuthGuard]},
  { path: 'applications/new', component: KennelNewComponent, canActivate: [AuthGuard] },
  { path: 'kennels', component: KennelListComponent, canActivate: [AuthGuard]},
  { path: 'kennels/new', component: KennelNewComponent, canActivate: [AuthGuard]},
  { path: 'kennels/applications', component: KennelApplicationComponent, canActivate: [AuthGuard]},
  { path: 'kennels/:id/edit', component: KennelEditComponent, canActivate: [AuthGuard]},
  { path: 'dogs', component: DogListComponent, canActivate: [AuthGuard]},
  { path: 'dogs/new', component: DogNewComponent, canActivate: [AuthGuard]},
  { path: 'dogs/:id/edit', component: DogEditComponent, canActivate: [AuthGuard]},
  { path: 'litters', component: LitterListComponent, canActivate: [AuthGuard]},
  { path: 'litters/new', component: LitterNewComponent, canActivate: [AuthGuard]},
  { path: 'collection', component: CollectionListComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    UsersComponent,
    UserListComponent,
    UserEditComponent,
    UserNewComponent,
    KennelApplicationComponent,
    KennelNewComponent,
    KennelListComponent,
    KennelEditComponent,
    DogListComponent,
    DogNewComponent,
    DogEditComponent,
    LitterListComponent,
    LitterNewComponent,
    CollectionListComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    TooltipModule.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AccordionModule.forRoot()
  ],
  providers: [UserService, KennelService, BreedService, DogService, AuthService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
