import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Dog} from '../../models/dog.model';
import {Router} from '@angular/router';
import {Kennel} from '../../models/kennel.model';
import {User} from '../../models/user.model';
import {AuthService} from '../../user/account/auth.service';
import {KennelService} from '../../services/kennel.service';

@Component({
  selector: 'app-litter-new',
  templateUrl: './litter-new.component.html',
  styleUrls: ['./litter-new.component.css']
})
export class LitterNewComponent implements OnInit {
  newLitterForm: FormGroup;
  currentUser: User;
  userKennel: Kennel;
  femaleDogs: Dog[];
  maleDogs: Dog[];
  isError = false;
  alertMessage: string;

  constructor(private router: Router, private authService: AuthService, private kennelService: KennelService) {}

  /** Initialize component */
  ngOnInit() {
    this.currentUser = this.authService.currentUser;
    this.getKennelByUser(this.currentUser.userName);
  }

  onSubmit() {
    // not implemented
  }

  /** Cancel adding new litter and navigate to litters page */
  onCancel() {
    this.router.navigate(['/litters']);
  }

  /** Get kennel per user */
  getKennelByUser(userName: string) {
    this.kennelService.getKennelByUser(userName)
      .subscribe(data => {
          this.userKennel = data;
        },
        error => console.log(error)
      );
  }
}
