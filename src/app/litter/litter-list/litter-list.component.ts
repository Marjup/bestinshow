import {Component, OnInit} from '@angular/core';
import {Kennel} from '../../models/kennel.model';
import {User} from '../../models/user.model';
import {Litter} from '../../models/litter.model';
import {AuthService} from '../../user/account/auth.service';
import {KennelService} from '../../services/kennel.service';

@Component({
  selector: 'app-litter-list',
  templateUrl: './litter-list.component.html',
  styleUrls: ['./litter-list.component.css']
})
export class LitterListComponent implements OnInit {
  kennel: Kennel;
  currentUser: User;
  litters: Litter[];

  constructor(private authService: AuthService, private kennelService: KennelService) {}

  /** Initialize component */
  ngOnInit() {
    this.currentUser = this.authService.currentUser;
    this.getKennelByUser(this.currentUser.userName);
  }

  onAddNew() {
    // not implemented
  }

  onDetails(id: number) {
    // not implemented
  }

  onEdit(id: number) {
    // not implemented
  }

  /** Get kennel and litter data per user */
  getKennelByUser(userName: string) {
    this.kennelService.getKennelByUser(userName)
      .subscribe(data => {
          this.kennel = data;
          this.litters = this.kennel.litters;
        },
        error => console.log(error)
      );
  }

}
