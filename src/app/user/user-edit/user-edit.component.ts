import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';
import {Subscription} from 'rxjs/index';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})

export class UserEditComponent implements OnInit {
  editForm: FormGroup;
  id: number;
  user: User;
  roles = ['owner', 'breeder', 'admin'];
  alertMessage: string;
  isError = false;
  httpSubscription: Subscription;

  constructor(private router: Router, private route: ActivatedRoute,
              private userService: UserService) {}

  /** Edit user and navigate to users page */
  onEdit(user: User) {
    this.httpSubscription = this.userService.editUser(user)
      .subscribe(
        () => {
          this.userService.message = 'Account successfully changed';
          this.isError = false;
          this.router.navigate(['/users']);
        },
        error => {
          this.alertMessage = error;
          console.log(error);
          this.isError = true;
        }
      );
  }

  /** Submit user edit data */
  onSubmit() {
    if (this.httpSubscription) {
      this.httpSubscription.unsubscribe();
    }
    this.editForm.markAsUntouched();

    // make user object
    this.user.firstName = this.editForm.get('firstname').value;
    this.user.lastName = this.editForm.get('lastname').value;
    this.user.role = this.editForm.get('role').value;

    this.onEdit(this.user);
  }

  /** Initialize component */
  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.user = this.userService.getUserById(this.id);
          this.isError = false;
          this.initForm();
        }
      );
  }

  /** Initialize the edit form with user data */
  private initForm() {
    this.editForm = new FormGroup({
      'username': new FormControl(this.user.userName),
      'firstname': new FormControl(this.user.firstName, [Validators.required, Validators.maxLength(30)]),
      'lastname': new FormControl(this.user.lastName, [Validators.required, Validators.maxLength(30)]),
      'code': new FormControl(this.user.code),
      'role': new FormControl(this.user.role, [Validators.required])
    });
  }

  /** Cancel editing user and navigate to users page */
  onCancel() {
    this.router.navigate(['/users']);
  }


}
