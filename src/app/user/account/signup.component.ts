import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user.model';
import {Subscription} from 'rxjs/index';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-account-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  signupSuccess = false;
  user: User;
  private status = 0;
  private role = '';
  alertMessage: string;
  isError = false;
  httpSubscription: Subscription;

  constructor(private userService: UserService, private router: Router) {}

  /** Initialize component */
  ngOnInit() {
    this.signupForm = new FormGroup({
      'firstname' : new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      'lastname' : new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      'code' : new FormControl(null, [Validators.required, Validators.maxLength(11), Validators.minLength(11)]),
      'username' : new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(40)]),
      'password' : new FormControl('', Validators.required),
      'passwordconfirm' : new FormControl('', [Validators.required])
    }, this.matchPasswords);
    this.signupSuccess = false;
  }

  /** Custom validator for password matching*/
  matchPasswords(group: FormGroup) {
    return group.get('password').value === group.get('passwordconfirm').value ? null : {'passwordsdontmatch': true};
  }

  /** Register new user */
  registerUser(user: User) {
    this.httpSubscription = this.userService.addUser(user)
      .subscribe(
        () => {
          this.alertMessage = 'New account successfully created';
          this.isError = false;
          this.signupForm.reset();
        },
        error => {
          this.alertMessage = error;
          this.isError = true;
        }
      );
  }

  /** Submit new user info from form */
  onSubmit() {
    if (this.httpSubscription) {
      this.httpSubscription.unsubscribe();
    }
    this.signupForm.markAsUntouched();

    // make user object
    this.user = new User (
      '',
      this.signupForm.get('firstname').value,
      this.signupForm.get('lastname').value,
      this.signupForm.get('username').value,
      this.signupForm.get('code').value,
      this.role,
      this.signupForm.get('password').value,
      this.status
    );

    this.registerUser(this.user);
    this.signupSuccess = true;
  }
}
