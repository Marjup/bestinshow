import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-account-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  currentUsername: string;
  isError = false;
  alertMessage: string;

  constructor(private authService: AuthService) {}

  /** Initialize component */
  ngOnInit() {
    this.loginForm = new FormGroup({
      'username' : new FormControl(null, [Validators.required, Validators.email]),
      'password' : new FormControl(null, Validators.required)
    });
  }

  /** Login user with username and password */
  login(username: string, password: string) {
    this.authService.login(username, password)
      .subscribe(
        () => {
          console.log('Success');
          this.currentUsername = this.authService.username;
          this.loginForm.reset();
        },
        error => {
          this.alertMessage = error;
          this.isError = true;
        }
      );
  }

  /** Submit login info */
    onSubmit() {
      this.loginForm.markAsUntouched();
      this.isError = false;
      this.login(this.loginForm.value['username'], this.loginForm.value['password']);
  }
}
