import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs/index';
import {catchError, tap} from 'rxjs/internal/operators';
import {User} from '../../models/user.model';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {
  private url = 'https://localhost:44343/api/Security/getToken';
  private urlUsers = 'https://localhost:44343/api/Users/';
  private url3 = 'https://localhost:44343/api/Security';
  username: string;
  currentUser: User;

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error
      console.log('An error occures:' + error.error);
    } else {
      // The backend returned an unsuccessful response code
      if (error.error.errors) {
        return throwError(error.error.errors[0]['description']);
      }
      return throwError(error.error);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened: please try again later.');
  }

  constructor(private http: HttpClient, private router: Router) {}

  /** Log user in and create token */
  login(userName: string, password: string) {
    var user = {
      'userName': userName,
      'password': password
    };

    this.username = userName;
    return this.http.post(this.url, user)
      .pipe(
        tap(
          data => {
            this.setSession(data['token']);
            this.setCurrentUser();

          }
        ),
        catchError(this.handleError)
      );
  }

  /** Log user out and remove token */
  logout() {
    return this.http.post( this.url3, this.currentUser)
      .subscribe(
        () => {
          console.log('User log out');
          localStorage.removeItem('token');
          this.username = null;
          this.currentUser = null;
          this.router.navigate(['/']);
        },
        error => console.log(error)
      );
  }

  /** Set current user after login */
  setCurrentUser() {
    this.http.get<User>(this.urlUsers + this.username)
      .subscribe(
        user => {
          this.currentUser = user;
          console.log(this.currentUser);
          if (this.currentUser.role === 'admin') {
            this.router.navigate(['/users']);
          } else {
            this.router.navigate(['/dogs']);
          }
        },
        error => console.log(error)
      );
  }

  /** Set session token */
  private setSession(authResult) {
    localStorage.setItem('token', authResult);
  }

  /** Get session token */
  public getToken(): string {
    return localStorage.getItem('token');
  }

  /** Check if token is present */
  isAuthenticated() {
    return this.getToken() != null;
  }

}
