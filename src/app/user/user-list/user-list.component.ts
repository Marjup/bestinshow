import {Component, Input, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  alertMessage: string;
  modalRef: BsModalRef;
  userOnChange: User;

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private modalService: BsModalService) {}

  /** Open modal */
  openModal(template: TemplateRef<any>, user: User) {
    this.modalRef = this.modalService.show(template);
    this.userOnChange = user;
  }

  /** Edit user status */
  onChangeStatus() {
    if (this.userOnChange.status === 0) {
      this.userOnChange.status = 1;
    } else {
      this.userOnChange.status = 0;
    }

    this.userService.editUser(this.userOnChange)
      .subscribe(
        data => {
          console.log(data);
          this.ngOnInit();
          this.modalRef.hide();
        }, error => {
          console.log(error);
          this.modalRef.hide();
        }
      );
  }

  /** Initialize component */
  ngOnInit() {
    this.alertMessage = this.userService.message;
    this.getUsers();
  }

  /** Destroy messages when destroying component */
  ngOnDestroy() {
    this.alertMessage = null;
    this.userService.message = null;
  }

  /** Close alert */
  onClosedAlert() {
    this.alertMessage = null;
    this.userService.message = null;
  }

  /** Get all user data */
  getUsers() {
    this.userService.getUsers()
      .subscribe(
        data => {
          this.users = data;
          this.userService.users = this.users;
          },
        err => console.log(err)
      );
  }

  /** Navigate to user edit form */
  onEditUser(id: number) {
    this.router.navigate([id, 'edit'], {relativeTo: this.route });
  }

  /** Navigate to new user form */
  onAddNew() {
    this.router.navigate(['new'], {relativeTo: this.route });
  }
}
