import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/index';
import {Router} from '@angular/router';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-user-new',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.css']
})
export class UserNewComponent implements OnInit {
  newUserForm: FormGroup;
  roles = ['owner', 'breeder', 'admin'];
  alertMessage: string;
  isError = false;
  httpSubscription: Subscription;
  private status = 0;
  user: User;

  constructor(private router: Router, private userService: UserService) {}

  /** Initialize component */
  ngOnInit() {
    this.newUserForm = new FormGroup({
      'username': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(40)]),
      'firstname': new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      'lastname': new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      'code': new FormControl(null, [Validators.required, Validators.maxLength(11), Validators.minLength(11)]),
      'role': new FormControl(null, [Validators.required]),
      'password': new FormControl('', Validators.required)
    });
  }

  /** Submit new user data from form */
  onSubmit() {
    if (this.httpSubscription) {
      this.httpSubscription.unsubscribe();
    }
    this.newUserForm.markAsUntouched();

    // make user object
    this.user = new User (
      '',
      this.newUserForm.get('firstname').value,
      this.newUserForm.get('lastname').value,
      this.newUserForm.get('username').value,
      this.newUserForm.get('code').value,
      this.newUserForm.get('role').value,
      this.newUserForm.get('password').value,
      this.status
    );

    this.registerUser(this.user);
  }

  /** Register new user and navigate to users page */
  registerUser(user: User) {
    this.httpSubscription = this.userService.addUser(user)
      .subscribe(
        () => {
          this.userService.message = 'New user successfully created';
          this.isError = false;
          this.router.navigate(['/users']);
        },
        error => {
          this.alertMessage = error;
          this.isError = true;
        }
      );
  }

  /** Cancel adding new user */
  onCancel() {
    this.router.navigate(['/users']);
  }
}
