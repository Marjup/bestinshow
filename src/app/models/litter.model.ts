export class Litter {
  public litterId: number;
  public litterName: string;
  public litterBirthTime: Date;
  public kennelId: number;
  public breedId: number;
  public motherDogId: number;
  public fatherDogId: number;

  constructor(id: number, name: string, birth: Date, kennel: number, breed: number, mother: number, father: number) {
    this.litterId = id;
    this.litterName = name;
    this.litterBirthTime = birth;
    this.kennelId = kennel;
    this.breedId = breed;
    this.motherDogId = mother;
    this.fatherDogId = father;
  }
}
