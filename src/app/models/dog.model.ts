import {Breed} from './breed.model';

export class Dog {
  public dogId: number;
  public name: string;
  public estMicrochip: string;
  public otherChip: string;
  public dogGender: string;
  public dogColor: string;
  public dogEyeColor: string;
  public dogDateOfBirth: Date;
  public dogDateOfDeath: Date;
  public breed: Breed;
  public ownerName: string;

  constructor(id: number, name: string, estMicrochip: string, otherChip: string, dogGender: string,
              dogColor: string, dogEyeColor: string, dogDateOfBirth: Date, dogDateOfDeath: Date,
              breed: Breed, ownerName: string) {
    this.dogId = id;
    this.name = name;
    this.estMicrochip = estMicrochip;
    this.otherChip = otherChip;
    this.dogGender = dogGender;
    this.dogColor = dogColor;
    this.dogEyeColor = dogEyeColor;
    this.dogDateOfBirth = dogDateOfBirth;
    this.dogDateOfDeath = dogDateOfDeath;
    this.breed = breed;
    this.ownerName = ownerName;
  }
}
