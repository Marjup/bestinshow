export class User {
  public userId: string;
  public userName: string;
  public firstName: string;
  public lastName: string;
  public code: string;
  public role: string;
  public password: string;
  public status: number;

  constructor(userid: string, firstname: string, lastname: string, username: string, code: string, role: string,
              password: string, status: number) {
    this.userId = userid;
    this.userName = username;
    this.firstName = firstname;
    this.lastName = lastname;
    this.code = code;
    this.role = role;
    this.password = password;
    this.status = status;
  }


}
