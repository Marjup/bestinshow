import {Breed} from './breed.model';
import {Litter} from './litter.model';

export class Kennel {
  public kennelId: number;
  public name: string;
  public address: string;
  public status: number;
  public user: string;
  public litters: Litter[];
  public breeds: Breed[];

  constructor(id: number, name: string, address: string, status: number, breeder: string,
              litters: Litter[], breeds: Breed[]) {
    this.kennelId = id;
    this.name = name;
    this.address = address;
    this.status = status;
    this.user = breeder;
    this.litters = litters;
    this.breeds = breeds;
  }
}
