export class Breed {
  public breedId: number;
  public FCIStandard: number;
  public breedNameEst: string;
  public breedNameEng: string;

  constructor(id: number, fci: number, nameEst: string, nameEng: string) {
    this.breedId = id;
    this.FCIStandard = fci;
    this.breedNameEst = nameEst;
    this.breedNameEng = nameEng;
  }
}
