import {Component, OnInit} from '@angular/core';
import {AuthService} from './user/account/auth.service';
import {User} from './models/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser: User;

  constructor(private authService: AuthService) {}

  /** Initialize component */
  ngOnInit() {
    this.getCurrentUser();
  }

  /** Get current user data */
  getCurrentUser() {
    this.currentUser = this.authService.currentUser;
  }

}
