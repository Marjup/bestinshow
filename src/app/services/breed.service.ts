import {Injectable, OnInit} from '@angular/core';
import {Breed} from '../models/breed.model';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs/index';
import {catchError} from 'rxjs/internal/operators';

@Injectable()
export class BreedService {
  private url = 'https://localhost:44343/api/Breeds';

  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error
      console.log('An error occures:' + error.error);
    } else {
      // The backend returned an unsuccessful response code
      if (error.error.errors) {
        return throwError(error.error.errors[0]['description']);
      }
      return throwError(error.error);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened: please try again later.');
  }

  /** Get all breeds */
  getBreeds() {
    return this.http.get<Breed[]>(this.url)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Get breed by id*/
  getBreedById(id: number) {
    return this.http.get(this.url + '/' + id)
      .pipe(
        catchError(this.handleError)
      );
  }

}
