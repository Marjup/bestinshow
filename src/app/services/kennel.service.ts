import {Injectable} from '@angular/core';
import {Kennel} from '../models/kennel.model';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs/index';
import {catchError} from 'rxjs/internal/operators';

@Injectable()
export class KennelService {
  private url = 'https://localhost:44343/api/Kennels';

  constructor(private http: HttpClient) {}


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error
      console.log('An error occures:' + error);
    } else {
      // The backend returned an unsuccessful response code
      if (error.error.errors) {
        return throwError(error.error.errors[0]['description']);
      }
      return throwError(error.error);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened: please try again later.');
  }

  /** Add a new kennel to the database */
  addKennel(kennel: Kennel): Observable<Kennel> {
    return this.http.post<Kennel>(this.url, kennel)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Get all kennels */
  getKennels() {
    return this.http.get<Kennel[]>(this.url)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Edit kennel */
  editKennel(kennel: Kennel) {
    return this.http.put<Kennel>(this.url + '/' +  kennel.kennelId, kennel)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Get kennel by ID*/
  getKennelById(id: number) {
    return this.http.get<Kennel>(this.url + '/' + id)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Get kennel by username*/
  getKennelByUser(userName: string) {
    return this.http.get<Kennel>(this.url + '/breeder/' + userName)
      .pipe(
        catchError(this.handleError)
      );
  }


}
