import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs/index';
import {Dog} from '../models/dog.model';
import {catchError} from 'rxjs/internal/operators';

@Injectable()
export class DogService {
  private url = 'https://localhost:44343/api/Dogs';

  constructor(private http: HttpClient) {}


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error
      console.log('An error occures:' + error);
    } else {
      // The backend returned an unsuccessful response code
      if (error.error.errors) {
        return throwError(error.error.errors[0]['description']);
      }
      return throwError(error.error);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened: please try again later.');
  }

  /** Get dogs by owner name */
  getDogsByOwner(userName: string) {
    return this.http.get<Dog[]>(this.url + '/owner/' + userName)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Add a new dog to the database */
  addDog(dog: object): Observable<Dog> {
    return this.http.post<Dog>(this.url, dog)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Get dog by ID*/
  getDogById(id: number) {
    return this.http.get<Dog>(this.url + '/' + id)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Edit dog */
  editDog(dog: Dog) {
    return this.http.put<Dog>(this.url + '/' +  dog.dogId, dog)
      .pipe(
        catchError(this.handleError)
      );
  }
}
