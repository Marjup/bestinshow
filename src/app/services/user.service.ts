import {User} from '../models/user.model';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs/index';
import {catchError} from 'rxjs/internal/operators';
import {Injectable} from '@angular/core';
import {AuthService} from '../user/account/auth.service';

@Injectable()
export class UserService {
  private url = 'https://localhost:44343/api/Users';
  users: User[];
  message: string;

  constructor(private http: HttpClient, private authService: AuthService) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error
      console.log('An error occures:' + error);
    } else {
      // The backend returned an unsuccessful response code
      if (error.error.errors) {
        return throwError(error.error.errors[0]['description']);
      }
      return throwError(error.error);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened: please try again later.');
  }

  /** Add a new user to the database */
  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.url, user)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Get all users */
  getUsers() {
    return this.http.get<User[]>(this.url)
      .pipe(
        catchError(this.handleError)
      );
  }

  /** Get user from the users list */
  getUserById(id: number) {
    return this.users[id];
  }

  /** Edit user */
  editUser(user: User) {
    return this.http.put<User>(this.url, user)
      .pipe(
        catchError(this.handleError)
      );
  }

}
