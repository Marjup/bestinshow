import {Component, OnInit} from '@angular/core';
import {Breed} from '../../models/breed.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {BreedService} from '../../services/breed.service';
import {AuthService} from '../../user/account/auth.service';
import {Kennel} from '../../models/kennel.model';
import {KennelService} from '../../services/kennel.service';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-kennel-new',
  templateUrl: './kennel-new.component.html',
  styleUrls: ['./kennel-new.component.css']
})
export class KennelNewComponent implements OnInit {
  newKennelAppForm: FormGroup;
  breeds: Breed[];
  breed: Breed;
  isError = false;
  alertMessage: string;
  kennel: Kennel;
  status = 0;
  user: User;
  users: User[];

  constructor(private router: Router, private breedService: BreedService,
              private authService: AuthService, private kennelService: KennelService,
              private userService: UserService) {}

  /** Initialize component */
  ngOnInit() {
    this.getBreeds();
    this.user = this.authService.currentUser;
    if (this.user.role === 'admin') {
      this.getUsers();
      this.newKennelAppForm = new FormGroup({
        'kennelname': new FormControl(null, [Validators.required, Validators.maxLength(100)]),
        'address': new FormControl(null, [Validators.required, Validators.maxLength(200)]),
        'breed': new FormControl(null, [Validators.required]),
        'breeder': new FormControl(null, [Validators.required])
      });
    } else {
      this.newKennelAppForm = new FormGroup({
        'kennelname': new FormControl(null, [Validators.required, Validators.maxLength(100)]),
        'address': new FormControl(null, [Validators.required, Validators.maxLength(200)]),
        'breed': new FormControl(null, [Validators.required])
      });
    }
  }

  /** Navigate to kennels or application page based on the currentuser role */
  onCancel() {
    if (this.user.role === 'admin') {
      this.router.navigate(['/kennels']);
    } else {
      this.router.navigate(['/applications']);
    }
  }

  /** Submit new kennel */
  onSubmit() {
    this.newKennelAppForm.markAsUntouched();
    let user;

    // get breed
    let id = +this.newKennelAppForm.get('breed').value;
    this.breed = this.breeds.find((data) =>  data.breedId === id);
    let breeds = [this.breed];

    // get user
    if (this.user.role === 'admin') {
      this.status = 1;
      user = this.newKennelAppForm.get('breeder').value;
    } else {
      this.status = 0;
      user = this.user.userName;
    }

    // make object
    this.kennel = new Kennel(
      0,
      this.newKennelAppForm.get('kennelname').value,
      this.newKennelAppForm.get('address').value,
      this.status,
      user,
      [],
      breeds
    );

    // send application/kennel
    this.addKennelApplication(this.kennel);
  }

  /** Add new kennel/application and navigate to kennels/applications page */
  addKennelApplication(kennel: Kennel) {
    return this.kennelService.addKennel(kennel)
      .subscribe(
        data => {
          if (this.user.role === 'admin') {
            this.userService.message = 'New kennel successfully added';
          } else {
            this.userService.message = 'New kennel application successfully sent';
          }
          this.isError = false;
          if (this.user.role === 'admin') {
            this.router.navigate(['/kennels']);
          } else {
            this.router.navigate(['/applications']);
          }
        },
            error => {
              this.alertMessage = error;
              this.isError = true;
            }
      );
  }

  /** Get all breed data */
  getBreeds() {
    return this.breedService.getBreeds()
      .subscribe(
        data => {
          this.breeds = data;
        },
        error => console.log(error)
      );
  }

  /** Get all users data */
  getUsers() {
    return this.userService.getUsers()
      .subscribe(
        data => {
          this.users = data;
        },
        error => console.log(error)
      );
  }
}
