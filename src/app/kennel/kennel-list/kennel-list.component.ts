import {Component, OnDestroy, OnInit} from '@angular/core';
import {Kennel} from '../../models/kennel.model';
import {KennelService} from '../../services/kennel.service';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-kennel-list',
  templateUrl: './kennel-list.component.html',
  styleUrls: ['./kennel-list.component.css']
})
export class KennelListComponent implements OnInit, OnDestroy {
  kennels: Kennel[];
  hasKennels = false;
  alertMessage: string;

  constructor(private kennelService: KennelService, private router: Router,
              private route: ActivatedRoute, private userService: UserService) {}

  /** Initialize component */
  ngOnInit() {
    this.alertMessage = this.userService.message;
    this.getAllKennels();
  }

  /** Destroy component  */
  ngOnDestroy() {
    this.alertMessage = null;
    this.userService.message = null;
  }

  /** Close alert message */
  onClosedAlert() {
    this.alertMessage = null;
    this.userService.message = null;
  }

  /** Navigate to applications page */
  openApplications() {
    this.router.navigate(['applications'], {relativeTo: this.route });
  }

  /** Navigate to add new kennel page */
  onAddNew() {
    this.router.navigate(['new'], {relativeTo: this.route });
  }

  onDetailsKennel(kennel: Kennel) {
    // not implemented
  }

  /** Navigate to edit kennel page */
  onEditKennel(id: number) {
    this.router.navigate([id, 'edit'], {relativeTo: this.route });
  }

  /** Get all kennels data */
  getAllKennels() {
    return this.kennelService.getKennels()
      .subscribe(data => {
          this.kennels = data.filter((kennel) => {
            return kennel.status === 1;
          });
          if (this.kennels) {
            this.hasKennels = true;
          } else {
            this.hasKennels = false;
          }
        },
        error => console.log(error)
      );
  }
}
