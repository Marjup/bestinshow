import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BreedService} from '../../services/breed.service';
import {Breed} from '../../models/breed.model';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {KennelService} from '../../services/kennel.service';
import {Kennel} from '../../models/kennel.model';

@Component({
  selector: 'app-kennel-edit',
  templateUrl: './kennel-edit.component.html',
  styleUrls: ['./kennel-edit.component.css']
})
export class KennelEditComponent implements OnInit {
  editForm: FormGroup;
  breeds: Breed[];
  breed: Breed;
  user: User;
  users: User[];
  kennel: Kennel;
  isError = false;
  alertMessage: string;
  id: number;


  constructor(private breedService: BreedService, private userService: UserService,
              private router: Router, private kennelService: KennelService,
              private route: ActivatedRoute) {}

  /** Initialize component */
  ngOnInit() {
    this.getBreeds();
    this.getUsers();
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
        }
        );
    this.getKennelById(this.id);
  }

  /** Get kennel by id and init form */
  getKennelById(id: number) {
    this.kennelService.getKennelById(id)
      .subscribe(data => {
          this.kennel = data;
          this.isError = false;
          this.initForm();
        },
        error => console.log(error)
      );
  }

  /** Initialize form with kennel data */
  private initForm() {
    this.editForm = new FormGroup({
      'kennelname': new FormControl(this.kennel.name, [Validators.maxLength(100)]),
      'address': new FormControl(this.kennel.address, [Validators.required, Validators.maxLength(200)]),
      'breeder': new FormControl(this.kennel.user, [Validators.required]),
      'breed': new FormControl(this.kennel.breeds[0].breedNameEng, [Validators.required]),
    });
  }

  /** Submit form with kennel edit data */
  onSubmit() {
    this.editForm.markAsUntouched();

    // make kennel object
    this.kennel.name = this.editForm.get('kennelname').value;
    this.kennel.address = this.editForm.get('address').value;
    this.kennel.user = this.editForm.get('breeder').value;

    // get breed
    let breedname = this.editForm.get('breed').value;
    this.breed = this.breeds.find((data) =>  data.breedNameEng === breedname);
    this.kennel.breeds = [this.breed];

    this.onEdit(this.kennel);
  }

  /** Cancel editing the kennel and navigate to kennels page */
  onCancel() {
    this.router.navigate(['/kennels']);
  }

  /** Edit kennel and navigate to kennels page */
  onEdit(kennel: Kennel) {
    this.kennelService.editKennel(kennel)
      .subscribe(
        data => {
          this.userService.message = 'Kennel successfully changed';
          this.isError = false;
          this.router.navigate(['/kennels']);
        }, error => {
          this.alertMessage = error;
          console.log(error);
          this.isError = true;
        }
      );
  }

  /** Get all breed data */
  getBreeds() {
    return this.breedService.getBreeds()
      .subscribe(
        data => {
          this.breeds = data;
        },
        error => console.log(error)
      );
  }

  /** Get all users data */
  getUsers() {
    return this.userService.getUsers()
      .subscribe(
        data => {
          this.users = data;
        },
        error => console.log(error)
      );
  }
}
