import {Component, OnInit, TemplateRef} from '@angular/core';
import {Kennel} from '../../models/kennel.model';
import {AuthService} from '../../user/account/auth.service';
import {KennelService} from '../../services/kennel.service';
import {forEach} from '@angular/router/src/utils/collection';
import {BreedService} from '../../services/breed.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../models/user.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-kennel-application',
  templateUrl: './kennel-application.component.html',
  styleUrls: ['./kennel-application.component.css']
})
export class KennelApplicationComponent implements OnInit {
  applications: Kennel[];
  user: User;
  hasApplications = false;
  modalRef: BsModalRef;
  applicationOnChange: Kennel;

  constructor(private authService: AuthService, private kennelService: KennelService,
              private breedService: BreedService, private router: Router,
              private route: ActivatedRoute, private modalService: BsModalService) {}

  /** Navigate to new kennel form */
  onAddNew() {
    this.router.navigate(['new'], {relativeTo: this.route });
  }

  /** Open modal */
  openModal(template: TemplateRef<any>, kennel: Kennel) {
    this.modalRef = this.modalService.show(template);
    this.applicationOnChange = kennel;
  }

  /** Initialize component */
  ngOnInit() {
    this.user = this.authService.currentUser;
    if (this.user.role === 'admin') {
      this.getAllPendingStatusKennels();
    } else {
      this.getAllKennels();
    }
  }

  /** Accept kennel application, hide modal and init component */
  onAccept() {
    this.applicationOnChange.status = 1;
    this.kennelService.editKennel(this.applicationOnChange)
      .subscribe(
        data => {
          this.ngOnInit();
          this.modalRef.hide();
        }, error => {
          console.log(error);
          this.modalRef.hide();
        }
      );
  }

  /** Reject application, hide modal and init component */
  onReject() {
    this.applicationOnChange.status = 2;
    this.kennelService.editKennel(this.applicationOnChange)
      .subscribe(
        data => {
          this.ngOnInit();
          this.modalRef.hide();
        }, error => {
          console.log(error);
          this.modalRef.hide();
        }
      );
  }

  /** Get all kennel data */
  getAllKennels() {
    return this.kennelService.getKennels()
      .subscribe(data => {
        this.applications = data.filter((kennel) => {
          return kennel.user === this.user.userName && (kennel.status === 0 || kennel.status === 2);
        });
          if (this.applications) {
            this.hasApplications = true;
          } else {
            this.hasApplications = false;
          }
        },
        error => console.log(error)
      );
  }

  /** Get all kennels with status 0 */
  getAllPendingStatusKennels() {
    return this.kennelService.getKennels()
      .subscribe(data => {
          this.applications = data.filter((kennel) => {
            return kennel.status === 0;
          });
          if (this.applications) {
            this.hasApplications = true;
          } else {
            this.hasApplications = false;
          }
        },
        error => console.log(error)
      );
  }

}
