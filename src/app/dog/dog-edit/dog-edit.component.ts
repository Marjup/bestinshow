import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {DogService} from '../../services/dog.service';
import {Dog} from '../../models/dog.model';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {AuthService} from '../../user/account/auth.service';
import {Breed} from '../../models/breed.model';
import {BreedService} from '../../services/breed.service';

@Component({
  selector: 'app-dog-edit',
  templateUrl: './dog-edit.component.html',
  styleUrls: ['./dog-edit.component.css']
})

export class DogEditComponent implements OnInit {
  editForm: FormGroup;
  currentUser: User;
  genders = ['F', 'M'];
  breeds: Breed[];
  breed: Breed;
  dog: Dog;
  isError = false;
  alertMessage: string;
  id: number;

  constructor(private route: ActivatedRoute, private dogService: DogService, private router: Router,
              private userService: UserService, private authService: AuthService, private breedService: BreedService) {}

  /** Initialize component */
  ngOnInit() {
    this.getBreeds();
    this.currentUser = this.authService.currentUser;
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
        }
      );
    this.getDogById(this.id);
  }

  /** Get dog by id number and initialize form */
  getDogById(id: number) {
    this.dogService.getDogById(id)
      .subscribe(data => {
          this.dog = data;
          this.isError = false;
          this.initForm();
        },
        error => console.log(error)
      );
  }

  /** Initialize form with dog data */
  private initForm() {
    if (this.currentUser.role === 'admin') {
      this.editForm = new FormGroup({
        'dogname': new FormControl(this.dog.name, [Validators.required, Validators.maxLength(100)]),
        'estMicrochip': new FormControl(this.dog.estMicrochip, [Validators.maxLength(20)]),
        'otherChip': new FormControl(this.dog.otherChip, [Validators.maxLength(20)]),
        'doggender': new FormControl(this.dog.dogGender, [Validators.required, Validators.maxLength(1)]),
        'color': new FormControl(this.dog.dogColor, [Validators.maxLength(20)]),
        'eyecolor': new FormControl(this.dog.dogEyeColor, [Validators.maxLength(20)]),
        'birthDate': new FormControl(this.dog.dogDateOfBirth, [Validators.required]),
        'deathDate': new FormControl(this.dog.dogDateOfDeath),
        'breed': new FormControl(this.dog.breed.breedId, [Validators.required])
      });
    } else {
      this.editForm = new FormGroup({
        'dogname': new FormControl(this.dog.name, [Validators.required, Validators.maxLength(100)]),
        'estMicrochip': new FormControl(this.dog.estMicrochip, [Validators.maxLength(20)]),
        'otherChip': new FormControl(this.dog.otherChip, [Validators.maxLength(20)]),
        'doggender': new FormControl(this.dog.dogGender, [Validators.required, Validators.maxLength(1)]),
        'color': new FormControl(this.dog.dogColor, [Validators.maxLength(20)]),
        'eyecolor': new FormControl(this.dog.dogEyeColor, [Validators.maxLength(20)]),
        'birthDate': new FormControl(this.dog.dogDateOfBirth, [Validators.required]),
        'deathDate': new FormControl(this.dog.dogDateOfDeath),
        'breed': new FormControl(this.dog.breed.breedNameEng, [Validators.required])
      });
    }
  }

  /** Submit form with dog edit data */
  onSubmit() {
    this.editForm.markAsUntouched();
    // make dog object for sending
    if (this.currentUser.role === 'admin') {
      let id = +this.editForm.get('breed').value;
      this.breed = this.breeds.find((data) =>  data.breedId === id);
      this.dog.name = this.editForm.get('dogname').value;
      this.dog.estMicrochip = this.editForm.get('estMicrochip').value;
      this.dog.otherChip = this.editForm.get('otherChip').value;
      this.dog.dogGender = this.editForm.get('doggender').value;
      this.dog.dogColor = this.editForm.get('color').value;
      this.dog.dogEyeColor = this.editForm.get('eyecolor').value;
      this.dog.dogDateOfBirth = this.editForm.get('birthDate').value;
      this.dog.dogDateOfDeath = this.editForm.get('deathDate').value;
      this.dog.breed = this.breed;
    } else {
      this.dog.dogColor = this.editForm.get('color').value;
      this.dog.dogEyeColor = this.editForm.get('eyecolor').value;
      this.dog.dogDateOfDeath = this.editForm.get('deathDate').value;
    }

    this.onEdit(this.dog);

  }

  /** Edit dog and navigate to dogs page */
  onEdit(dog: Dog) {
    this.dogService.editDog(dog)
      .subscribe(
        data => {
          console.log(data);
          this.userService.message = 'Dog successfully changed';
          this.isError = false;
          this.router.navigate(['/dogs']);
        }, error => {
          this.alertMessage = error;
          this.isError = true;
        }
      );

  }

  /** Cancel editing the dog and navigate to dogs page */
  onCancel() {
    this.router.navigate(['/dogs']);
  }

  /** Get all breed data */
  getBreeds() {
    return this.breedService.getBreeds()
      .subscribe(
        data => {
          this.breeds = data;
        },
        error => console.log(error)
      );
  }

}
