import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {Dog} from '../../models/dog.model';
import {UserService} from '../../services/user.service';
import {DogService} from '../../services/dog.service';
import {AuthService} from '../../user/account/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {User} from '../../models/user.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-dog-list',
  templateUrl: './dog-list.component.html',
  styleUrls: ['./dog-list.component.css']
})
export class DogListComponent implements OnInit, OnDestroy {
  dogs: Dog[];
  dogOnChange: Dog;
  editForm: FormGroup;
  currentUser: User;
  users: User[];
  hasDogs = false;
  modalRef: BsModalRef;
  alertMessage: string;
  oneAtATime = true;
  userDogs: {user: User, dogs: Dog[]}[] = [];

  constructor(private userService: UserService, private dogService: DogService, private authService: AuthService,
              private router: Router, private route: ActivatedRoute, private modalService: BsModalService) {}

  /** Initialize component */
  ngOnInit() {
    this.userDogs = [];
    this.currentUser = this.authService.currentUser;
    this.alertMessage = this.userService.message;
    if (this.currentUser.role !== 'admin') {
      this.getDogsByUser(this.currentUser.userName);
    }
    this.editForm = new FormGroup({
      'owner': new FormControl(null, [Validators.required])
    });
  }

  openModal(template: TemplateRef<any>, dog: Dog) {
    this.editForm = new FormGroup({
      'owner': new FormControl(null, [Validators.required])
    });
    this.modalRef = this.modalService.show(template);
    this.dogOnChange = dog;
  }

  onAccept() {
    this.dogOnChange.ownerName = this.editForm.get('owner').value;
    this.dogService.editDog(this.dogOnChange)
      .subscribe(
        data => {
          this.ngOnInit();
          this.modalRef.hide();
        }, error => {
          console.log(error);
          this.modalRef.hide();
        }
      );
  }

  ngOnDestroy() {
    this.userDogs = [];
    this.alertMessage = null;
    this.userService.message = null;
  }

  onClosedAlert() {
    this.alertMessage = null;
    this.userService.message = null;
    this.ngOnInit();
  }


  onAddNew() {
    this.router.navigate(['new'], {relativeTo: this.route });
  }

  onDetailsDog(id: number) {
    // not implemented
  }

  onEditDog(id: number) {
    this.router.navigate([id, 'edit'], {relativeTo: this.route });
  }

  getDogsByUser(userName: string) {
    return this.dogService.getDogsByOwner(userName)
      .subscribe(data => {
          this.dogs = data;
          if (this.dogs) {
            this.hasDogs = true;
          } else {
            this.hasDogs = false;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  getAllUserDogs(user: User) {
    this.dogService.getDogsByOwner(user.userName)
      .subscribe(data => {
        this.userDogs.push({user: user, dogs: data});
        console.log(this.userDogs);
      },
      error => console.log(error));
  }

}
