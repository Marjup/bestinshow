import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Breed} from '../../models/breed.model';
import {BreedService} from '../../services/breed.service';
import {Router} from '@angular/router';
import {User} from '../../models/user.model';
import {AuthService} from '../../user/account/auth.service';
import {Dog} from '../../models/dog.model';
import {DogService} from '../../services/dog.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-dog-new',
  templateUrl: './dog-new.component.html',
  styleUrls: ['./dog-new.component.css']
})
export class DogNewComponent implements OnInit {
  newDogForm: FormGroup;
  genders = ['F', 'M'];
  breeds: Breed[];
  breed: Breed;
  users: User[];
  currentUser: User;
  isError = false;
  alertMessage: string;


  constructor(private breedService: BreedService, private router: Router, private authService: AuthService,
              private dogService: DogService, private userService: UserService) {}

  /** Initialize component */
  ngOnInit() {
    this.getBreeds();
    this.getUsers();
    this.currentUser = this.authService.currentUser;
    if (this.currentUser.role === 'admin') {
      this.newDogForm = new FormGroup({
        'dogname': new FormControl(null, [Validators.required, Validators.maxLength(100)]),
        'estMicrochip': new FormControl(null, [Validators.maxLength(20)]),
        'otherChip': new FormControl(null, [Validators.maxLength(20)]),
        'doggender': new FormControl(null, [Validators.required, Validators.maxLength(1)]),
        'color': new FormControl(null, [Validators.maxLength(20)]),
        'eyecolor': new FormControl(null, [Validators.maxLength(20)]),
        'birthDate': new FormControl(null, [Validators.required]),
        'breed': new FormControl(null, [Validators.required]),
        'owner': new FormControl(null, [Validators.required])
      });
    } else {
      this.newDogForm = new FormGroup({
        'dogname': new FormControl(null, [Validators.required, Validators.maxLength(100)]),
        'estMicrochip': new FormControl(null, [Validators.maxLength(20)]),
        'otherChip': new FormControl(null, [Validators.maxLength(20)]),
        'doggender': new FormControl(null, [Validators.required, Validators.maxLength(1)]),
        'color': new FormControl(null, [Validators.maxLength(20)]),
        'eyecolor': new FormControl(null, [Validators.maxLength(20)]),
        'birthDate': new FormControl(null, [Validators.required]),
        'breed': new FormControl(null, [Validators.required])
      });
    }
  }

  /** Submit form with new dog data */
  onSubmit() {
    this.newDogForm.markAsUntouched();
    let user;

    // find breed
    let id = +this.newDogForm.get('breed').value;
    this.breed = this.breeds.find((data) =>  data.breedId === id);

    // find user
    if (this.currentUser.role === 'admin') {
      user = this.newDogForm.get('owner').value;
    } else {
      user = this.currentUser.userName;
    }

    // make object
    let dog = {
      name: this.newDogForm.get('dogname').value,
      estMicrochip: this.newDogForm.get('estMicrochip').value,
      otherChip: this.newDogForm.get('otherChip').value,
      dogGender: this.newDogForm.get('doggender').value,
      dogColor: this.newDogForm.get('color').value,
      dogEyeColor: this.newDogForm.get('eyecolor').value,
      dogDateOfBirth: this.newDogForm.get('birthDate').value,
      breed: this.breed,
      ownerName: user
    };

    this.addNewDog(dog);
}

  /** Cancel editing the dog and navigate to dogs page */
  onCancel() {
    this.router.navigate(['/dogs']);
  }

  /** Add new dog and navigate to dogs page */
  addNewDog(dog: object) {
    return this.dogService.addDog(dog)
      .subscribe(
        data => {
            this.userService.message = 'New dog successfully added';
            this.isError = false;
            this.router.navigate(['/dogs']);
        },
        error => {
          this.alertMessage = error;
          this.isError = true;
        }
      );
  }

  /** Get all breed data */
  getBreeds() {
    return this.breedService.getBreeds()
      .subscribe(
        data => {
          this.breeds = data;
        },
        error => console.log(error)
      );
  }

  getUsers() {
    return this.userService.getUsers()
      .subscribe(
        data => {
          this.users = data.filter((user) => {
            return user.role !== 'admin';
          });
        },
        error => console.log(error)
      );
  }

}
