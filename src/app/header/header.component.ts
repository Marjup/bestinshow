import {Component} from '@angular/core';
import {AuthService} from '../user/account/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private authService: AuthService) {}

  /** Logout button method
   *  User is logged out
   * */
  onLogout() {
    this.authService.logout();
  }

}
