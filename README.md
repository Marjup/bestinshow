# Vr2Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Kuidas klient koos api-ga tööle saada?

1. Vaata, kas su masinas on node.js ja npm installitud:
    **node -v**
    **npm -v**
    
2. Kui sul ei ole node.js, siis installi see (npm tuleb automaatselt kaasa): https://nodejs.org/en/download/

3. Tõmba alla projekti kõik vajalikud sõltuvused:
    mine terminalis projekti juurkausta ja käivita käsk **npm install**
    
4. Pane käima API

5. Pane käima klient:
  mine terminalis projekti juurkausta ja käivita käsk **ng serve**
  Klient käivitub aadressil `http://localhost:4200/`
  
